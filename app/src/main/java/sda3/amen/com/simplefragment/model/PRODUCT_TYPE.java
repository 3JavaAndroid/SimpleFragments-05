package sda3.amen.com.simplefragment.model;

/**
 * Created by amen on 6/3/17.
 */

public enum PRODUCT_TYPE {
    CLOTH,
    ALCOHOL,
    FOOD,
    INDUSTRIAL
}
