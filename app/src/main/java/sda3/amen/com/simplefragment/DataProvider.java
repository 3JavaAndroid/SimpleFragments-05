package sda3.amen.com.simplefragment;

import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import sda3.amen.com.simplefragment.model.Product;

/**
 * Created by amen on 6/3/17.
 */

public enum DataProvider {
    INSTANCE;

    // provider przechowuje listę produktów
    private List<Product> list;

    // w konstruktorze tworzymy pustą listę
    DataProvider() {
        this.list = new LinkedList<>();
    }

    /**
     * Dodanie produktu do listy.
     *
     * @param product - produkt który ma być dodany
     */
    public void addProduct(Product product) {
        list.add(product);
    }

    public void removeProduct(Product p) {
        list.remove(p);
    }

    public List<Product> getProducts() {
        return list;
    }

    public void loadProducts() throws JSONException, IOException {
        list.clear();

        // czytamy całą zawartość pliku
        StringBuilder builder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/ProductsList.json"))) {
            builder.append(bufferedReader.readLine());
        }
        String buffer = builder.toString();

        // stworzenie obiektu gson - serializera
        GsonBuilder gBuilder = new GsonBuilder();
        Gson gson = gBuilder.create();

        // tworzymy tablice jsonów
        JSONArray array = new JSONArray(buffer);
        // iterujemy JSONObjecty z tablicy
        for (int i = 0; i < array.length(); i++) {
            // wybieram i-ty element z array
            JSONObject object = array.getJSONObject(i);

            // konwertuje go na Product
            Product p = gson.fromJson(object.toString(), Product.class);

            //dodanie do listy
            list.add(p);
        }

        // koniec
    }

    public void saveProducts() throws JSONException {
        // stworzenie obiektu gson - serializera
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        // tworzymy tablice jsonów
        JSONArray array = new JSONArray();

        // dodajemy obiekty do tablicy
        for (Product product : list) {
            JSONObject singleProduct = new JSONObject(gson.toJson(product));

            array.put(singleProduct);
        }

        // Zapis do pliku
        try (PrintWriter printWriter = new PrintWriter(
                new FileOutputStream(
                        Environment.getExternalStorageDirectory().getAbsolutePath()
                                + "/ProductsList.json"))) {
            printWriter.print(array.toString());
        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }
    }
}
